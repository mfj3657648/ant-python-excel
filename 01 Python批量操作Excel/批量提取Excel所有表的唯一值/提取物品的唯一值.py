

import pandas as pd

df_list = pd.read_excel("采购表.xlsx", sheet_name=None)

df_all = pd.concat(df_list.values())

df_names = pd.DataFrame({"物品": list(df_all["采购物品"].unique())})
df_names.to_excel("采购表-物品列表.xlsx", index=False)
