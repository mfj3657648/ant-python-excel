# pip install docxtpl

import pandas as pd
from docxtpl import DocxTemplate
import datetime

df = pd.read_excel("数据-学生成绩表.xlsx", index_col='姓名')
print(df.head(3))

pdate = datetime.datetime.now().strftime("%Y-%m-%d")
for name, row in df.iterrows():
    print("处理学生：", name)
    doc = DocxTemplate("模板-学生成绩单.docx")
    doc.render(dict(
        姓名=name,
        语文=row["语文"],
        数学=row["数学"],
        英语=row["英语"],
        总分=row["语文"] + row["数学"] + row["英语"],
        日期=pdate
    ))
    doc.save(f"./数据输出/学生成绩单-{name}.docx")
