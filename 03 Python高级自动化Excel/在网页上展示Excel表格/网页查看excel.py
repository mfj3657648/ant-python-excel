
import flask
import pandas as pd
app = flask.Flask(__name__)

@app.route("/")
def show_excel():
    df = pd.read_excel("按月采购表.xlsx")
    return f"""
        <html><body>
        <h1>按月采购表</h1>
        %s
        </body></html>
    """ % df.to_html()

app.run()
