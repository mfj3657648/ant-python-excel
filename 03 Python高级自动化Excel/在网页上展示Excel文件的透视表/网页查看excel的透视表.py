

import flask
import pandas as pd

app = flask.Flask(__name__)


@app.route("/excel")
def show_excel():
    df = pd.read_excel("按月采购表.xlsx")
    df_pivot = pd.pivot_table(
        df,
        index="月份",
        columns="采购物品",
        values="采购金额",
        fill_value=0.0
    )
    return f"""
        <html><body>
        <h1>按月采购表 - 透视表</h1>
        %s
        </body></html>
    """ % df_pivot.to_html()


app.run()
