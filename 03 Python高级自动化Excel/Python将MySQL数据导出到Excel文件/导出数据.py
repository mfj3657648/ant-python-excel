

import pandas as pd
import pymysql

conn = pymysql.connect(
    host='127.0.0.1',
    user='root',
    passwd='12345678',
    port=3306,
    db='db_school',
    charset='utf8'
)

df = pd.read_sql("""
    select * from student_grade
""", con=conn)
df.to_excel("学生成绩表.xlsx", index=False)

df = pd.read_sql("""
    select quarter, course, max(grade), min(grade), avg(grade)
    from student_grade
    group by quarter, course
""", con=conn).pivot(index="quarter", columns='course')
df.to_excel("学生成绩表-统计表.xlsx")
